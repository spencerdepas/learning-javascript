//reverseString
function reverseString(str) {

    var newString = "";
    for(var i = str.length -1; i >= 0; i --){
        newString += str[i];
    }
  
    return newString;
  } 
  var rStr = reverseString("bacon")


//Counts the number of individual words in a string
function countWords(str) {
    var wordCount = 1;
    for(var i =  0; i < str.length; i ++){
        if(i > 0 && str[i] == " " && str[i -1] != " "){
            wordCount ++;
        }
    }
    return wordCount;
  } 
  
  var wordCount = countWords("Hello, I am working on an education product. I think it will be awesome. I have made it how I would want it. It is a monthly product.")